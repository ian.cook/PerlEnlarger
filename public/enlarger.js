$(document).ready(function (){
    $('#safelight').click(function(){ post('/safelight',{});}); 
    $('#focus').click(function()    { post('/focus',{});    });
    $('select').selectmenu();
    $('input.num').keypad({keypadClass: 'darkKeypad'});
    $('input.text').keypad({keypadClass: 'darkKeypad',layout: [
        '123456780'+$.keypad.BACK,
        'qwertyuiop\'"',
        'asdfghjkl;:',
        'zxcvbnm,.?+-',
        $.keypad.SPACE_BAR + $.keypad.SHIFT + $.keypad.CLOSE]});
    $('input.decimal').keypad({keypadClass: 'darkKeypad',layout: [
        '123' + $.keypad.CLOSE,
        '456' + $.keypad.CLEAR,
        '789' + $.keypad.BACK,
        '.0,']});
    $('.aperture').click(function(){
            post('/prog/update',{
                type: "aperture",
                aperture: $(this).data('aperture'),
            },update_time_display);
        });
});

function post(url,data,callback){
    $('body').addClass('waiting');
    $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: "json"
    }).done(function(json){
        $('body').removeClass('waiting');
        $('#next').prop('disabled',false).removeClass('active');
        if (json.redirect){
            var now = new Date;
            var url;
            if (json.redirect.indexOf('#') > 0){
                var i = json.redirect.indexOf('#');
                url   = json.redirect.substring(0,i) + '?t=' + now.getTime() + json.redirect.substring(i,999);
            }
            else {
                url = json.redirect + '?t=' + now.getTime();
            }
            window.location.href = url;
        }
        else {
            $.each( json, function( key, value ) {
                var target = $('#'+key);

                if (key == 'safelight' || key == 'focus'){
                    if (value){
                        target.addClass('active');
                    }
                    else {
                        target.removeClass('active');
                    }
                }
                else if (key == 'aperture'){
                    $('.aperture').removeClass('active');                    
                    $('#apt-' + value.replace('.','_')).addClass('active');
                }
                else if (key == 'green_disable')    { prog_green_disable = value; }
                else if (key == 'blue_disable')     { prog_blue_disable  = value; }
                else if (key == 'blue_time')        { prog_blue_time     = value; }
                else if (key == 'green_time')       { prog_green_time    = value; }  
                else if (key == 'test_green_start') { test_green_start   = value; } 
                else if (key == 'test_green_end')   { test_green_end     = value; } 
                else if (key == 'test_green_mode')  { test_green_mode    = value; } 
                else if (key == 'test_blue_start')  { test_blue_start    = value; } 
                else if (key == 'test_blue_end')    { test_blue_end      = value; } 
                else if (key == 'test_blue_mode')   { test_blue_mode     = value; }                 else if (key == 'test_blue_mode')   { test_blue_mode     = value; } 
                else if (key == 'run_step')         { run_step           = value; }                 else if (key == 'test_blue_mode')   { test_blue_mode     = value; } 

                else if (target.is('input') || target.is('select')){
                    target.val(value);
                }            
                else {
                    target.text(value);
                }
            }); 
            if (callback){
                callback();
            }
        }
    }).fail(function(){});
}
