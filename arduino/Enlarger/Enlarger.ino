#include "math.h"
#include "limits.h"
#define BUFFER_SIZE 100
#define START_CHAR  0x3C
#define END_CHAR    0x3E
#define ACTIVE      8


char    recv[BUFFER_SIZE];
char    buffer[255];
byte    pins[ACTIVE]   = {5,10,7,12,9,11,8,6};
boolean cmd_read  = false;
boolean in_recv   = false;
byte    idx       = 0;
byte    cur_state = 0;


void setup() {
    Serial.begin(9600);
    while (!Serial) { }    
    Serial.println("Hello\n");
    pinMode(LED_BUILTIN,OUTPUT);
    digitalWrite(LED_BUILTIN,LOW);

    for (int i = 0;i < ACTIVE;i++){
        pinMode(pins[i], OUTPUT);
        digitalWrite(pins[i],LOW);
    }
}    

void loop() {
    cmd_recv();
    if (cmd_read){
        cmd_run();
    }
    delay(100);
}

void cmd_recv() {
    char c;
    char d[3];
 
    while (Serial.available() > 0 && !cmd_read) {
        c = Serial.read();    

        if (in_recv) {
            if (c != END_CHAR && idx < BUFFER_SIZE) {
                recv[idx++] = c;
            }
            else {
                in_recv   = false;
                cmd_read  = true;
            }
        }
        else if (c == START_CHAR) {
            in_recv = true;
            idx     = 0;
            memset(recv,0,BUFFER_SIZE);
        }
    }
}

void cmd_run() {
    unsigned long int offset;
    char cmd;
    int  state;
    unsigned long int run_time;

    memset(buffer,0,255);
    sscanf(recv,"%c %d %lu",&cmd,&state,&run_time);


    if (cmd == 'S'){
        sprintf(buffer,"OK %d",state);
        set_state(state);
    }
    else if (cmd == 'T'){        
        sprintf(buffer,"OK %d %lu",state,run_time);    
        run_exposure(state,run_time);      
    }
    else if (cmd == '?'){        
        for (int i = 0; i < 8;i++){
            buffer[i] = (cur_state & 1 << i) ? 0x31 : 0x30;
        }            
    }
    Serial.println(buffer);
    cmd_read = false;
}

void run_exposure(int state,unsigned long int offset) {
    int old_state = cur_state;
    digitalWrite(LED_BUILTIN,HIGH);
    set_state(state);
    delay(offset);
    set_state(old_state);
    digitalWrite(LED_BUILTIN,LOW);
}

void set_state(int state){
    cur_state = state;
    for (byte i=0;i<8;i++){
        digitalWrite(pins[i],(state & 1 << i) ? HIGH : LOW);        
    }
}


