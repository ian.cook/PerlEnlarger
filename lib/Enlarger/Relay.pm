package Enlarger::Relay;

use Moose;
use Enlarger::Channels;
use Enlarger::Relay::Test;
use Enlarger::Relay::USB;
use Enlarger::Relay::Arduino;

has config  => (is => 'rw', isa => 'HashRef');
has current => (is => 'rw', isa => 'Maybe[Int]');
has driver  => (is => 'rw');

sub init {
    my $self   = shift;
    my $config = shift;
    my $schema = shift;
    
    $self->config($config->{$config->{active}});

    my $relay_class = 'Enlarger::Relay::'.$config->{active};
    $self->driver($relay_class->new({
            config => $self->config,
            schema => $schema
        }));
    $self->driver->init();

    $self->driver->on(Enlarger::Channels::FAN() | Enlarger::Channels::SAFELIGHT() | Enlarger::Channels::FOCUS());
    return $self;
}


sub get_safelight { shift->_get(Enlarger::Channels::SAFELIGHT());         }
sub get_focus     { shift->_get(Enlarger::Channels::FOCUS());             }
sub safelight { shift->_set(Enlarger::Channels::SAFELIGHT(), shift);      }
sub focus     { shift->_set(Enlarger::Channels::FOCUS(), shift);          }

sub expose_seq {
    my $self  = shift;
    my $green = shift || 0;
    my $blue  = shift || 0;
    my $pre   = shift || 0;
    my $post  = shift || 0;

    $self->_time(Enlarger::Channels::PREFLASH(),  $pre);
    $self->_time(Enlarger::Channels::GREEN(),     $green);
    $self->_time(Enlarger::Channels::BLUE(),      $blue);
    $self->_time(Enlarger::Channels::POSTFLASH(), $post);

}

sub expose_con {
    my $self  = shift;
    my $green = shift || 0;
    my $blue  = shift || 0;
    my $pre   = shift || 0;
    my $post  = shift || 0;

    $self->_time(Enlarger::Channels::PREFLASH(),  $pre);

    if ($green > $blue){
        $self->_time(Enlarger::Channels::GREEN() | Enlarger::Channels::BLUE(), $blue);
        $self->_time(Enlarger::Channels::GREEN() , $green - $blue);
    }
    else {
        $self->_time(Enlarger::Channels::GREEN() | Enlarger::Channels::BLUE(), $green);
        $self->_time(Enlarger::Channels::BLUE() , $blue - $green);
    }

    $self->_time(Enlarger::Channels::POSTFLASH(), $post);
}

sub _set {
    my $self  = shift;
    my $type  = shift;
    my $state = shift;

    my $current = $self->driver->status();


    if ($state){
        $current |= $type;
    }
    else {
        $current &= ~$type;
    }

    $self->driver->on($current);
}

sub _get {
    my $self = shift;
    my $type = shift;

    my $current = $self->driver->status();

    return ($current & $type) ? 1 : 0;
}

sub _time {
    my $self    = shift;
    my $type    = shift;
    my $time    = shift;

    if ($time){
        $self->driver->time(Enlarger::Channels::FAN() | $type, $time);
    }
}




1;
