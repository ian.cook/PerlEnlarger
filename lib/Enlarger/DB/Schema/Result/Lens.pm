use utf8;
package Enlarger::DB::Schema::Result::Lens;

use Moose;
extends qw/Enlarger::DB::Schema::Base/;

__PACKAGE__->table('lens');
__PACKAGE__->add_columns(
        'id',
        { data_type => 'integer', is_auto_increment => 1, is_nullable => 0 },
        'name',
        { data_type => 'text', is_nullable => 0 },    
    );
__PACKAGE__->set_primary_key('id');
__PACKAGE__->has_many(
        'apertures' => 'Enlarger::DB::Schema::Result::Aperture',
        'lens_id'
    );
__PACKAGE__->has_many(
    'exposures' => 'Enlarger::DB::Schema::Result::Exposure',
    'lens_id'
);
1;
