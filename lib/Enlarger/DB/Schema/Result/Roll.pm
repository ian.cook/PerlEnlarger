use utf8;
package Enlarger::DB::Schema::Result::Roll;

use Moose;
extends qw/Enlarger::DB::Schema::Base/;

__PACKAGE__->table('roll');
__PACKAGE__->add_columns(
        'id',
        { data_type => 'integer', is_auto_increment => 1, is_nullable => 0 },
        'film_id',
        { data_type => 'integer', is_nullable => 0 },
        'ei',
        { data_type => 'integer', is_nullable => 0 },
    );
__PACKAGE__->set_primary_key('id');
__PACKAGE__->belongs_to(
    film => 'Enlarger::DB::Schema::Result::Film',
    'film_id'
);
__PACKAGE__->has_many(
    'exposures' => 'Enlarger::DB::Schema::Result::Exposure',
    'roll_id'
);
1;
