use utf8;
package Enlarger::DB::Schema::Result::Exposure;

use Moose;
extends qw/Enlarger::DB::Schema::Base/;
__PACKAGE__->table('exposure');
__PACKAGE__->add_columns(
        'id',
        { data_type => 'integer', is_auto_increment => 1, is_nullable => 0 },
        'timestamp',
        { data_type => 'integer', is_nullable => 1 },
        'roll_id',
        { data_type => 'integer', is_nullable => 1 },
        'frame_id',
        { data_type => 'integer', is_nullable => 1 },
        'lens_id',
        { data_type => 'integer', is_nullable => 1 },
        'aperture',
        { data_type => 'float', is_nullable => 1 },
        'height',
        { data_type => 'float', is_nullable => 1 },
        'blue',
        { data_type => 'text', is_nullable => 1, serializer_class   => 'JSON' },
        'green',
        { data_type => 'text', is_nullable => 1, serializer_class   => 'JSON' },
        'preflash',
        { data_type => 'float' },
        'postflash',
        { data_type => 'float' },
        'saved',
        { data_type => 'integer', is_nullable => 0, default => 0 },
);
__PACKAGE__->resultset_class('Enlarger::DB::Schema::ResultSet::Exposure');
__PACKAGE__->set_primary_key('id');
__PACKAGE__->belongs_to(
        'roll' => 'Enlarger::DB::Schema::Result::Roll',
        'roll_id'
    );
__PACKAGE__->belongs_to(
        'roll' => 'Enlarger::DB::Schema::Result::Roll',
        'roll_id'
    );

1;
