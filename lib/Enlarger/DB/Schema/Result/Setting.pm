use utf8;
package Enlarger::DB::Schema::Result::Setting;

use Moose;
use JSON;
extends qw/Enlarger::DB::Schema::Base/;

__PACKAGE__->table('setting');
__PACKAGE__->add_columns(
        'name',
        { data_type => 'text', is_nullable => 0 },
        'value',
        { data_type => 'text', is_nullable => 0 }, 
        'json', 
        { data_type => 'integer', is_nullable => 0},
        'comment',
        { data_type => 'text', is_nullable => 1 }
    );
__PACKAGE__->set_primary_key('name');

sub decode {
    my $self = shift;

    return $self->json ? JSON::decode_json($self->value) : $self->value;
}

sub encode {
    my $self = shift;
    my $val  = shift;

    if ($self->json){
        my @out = grep { defined $_ } @{$val};
        $self->value(JSON::encode_json(\@out));
    }
    else {
        $self->value($val);
    }
}

1;
