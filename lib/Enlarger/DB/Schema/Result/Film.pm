use utf8;
package Enlarger::DB::Schema::Result::Film;

use Moose;
extends qw/Enlarger::DB::Schema::Base/;

__PACKAGE__->table('film');
__PACKAGE__->add_columns(
        'id',
        { data_type => 'integer', is_auto_increment => 1, is_nullable => 0 },
        'name',
        { data_type => 'text', is_nullable => 0 },
        'iso',
        { data_type => 'integer', is_nullable => 0 },
    );
__PACKAGE__->set_primary_key('id');
__PACKAGE__->has_many(
    'rolls' => 'Enlarger::DB::Schema::Result::Roll',
    'film_id'
);

1;
