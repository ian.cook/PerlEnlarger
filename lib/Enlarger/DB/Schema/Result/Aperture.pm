use utf8;
package Enlarger::DB::Schema::Result::Aperture;

use Moose;
extends qw/Enlarger::DB::Schema::Base/;

__PACKAGE__->table('aperture');
__PACKAGE__->add_columns(
        'lens_id',
        { data_type => 'integer', is_auto_increment => 1, is_nullable => 0 },
        'aperture',
        { data_type => 'real', is_nullable => 0 },
    );
__PACKAGE__->set_primary_key(qw/lens_id aperture/);
__PACKAGE__->belongs_to(
        'lens' => 'Enlarger::DB::Schema::Result::Lens',
        'lens_id'
    );

1;
