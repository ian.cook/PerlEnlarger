package Enlarger::DB::Schema::ResultSet::Exposure;
 
use strict;
use warnings;
 
use base 'DBIx::Class::ResultSet';

sub get_saved {
    my $self  = shift;
    my $roll  = shift;
    my $frame = shift;
    my $limit = shift;

    my @saved = $self->search({
            roll_id  => $roll->id,
            frame_id => $frame,
            saved    => 1
        },{
            order_by => { -desc => 'id' },
            rows     => $limit
        })->all;
    if (scalar @saved < 15){
        $limit -= scalar @saved;
        my @previous = $self->search({
            roll_id  => $roll->id,
            frame_id => $frame,
            saved    => 0
        },{
            order_by => { -desc => 'id' },
            rows     => $limit
        })->all;
        push @saved, @previous;
    }
    return \@saved;
}

1;
