use utf8;
package Enlarger::DB::Schema::Base;

use Moose;
extends qw/DBIx::Class/;

__PACKAGE__->load_components('InflateColumn::Serializer', 'Core','InflateColumn::DateTime');

1;
