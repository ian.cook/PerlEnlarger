package Enlarger::DB::Connect;

use Moose;
use Enlarger::DB::Schema;
use Text::CSV qw/csv/;


sub connect {
    my $self = shift;
    my $home = shift;
    my $file = shift;

    my $db_file = $home->rel_file('data/'.$file);
    my $exists  = -e $db_file;

    my $schema = Enlarger::DB::Schema->connect('dbi:SQLite:'.$db_file);
    if (!$exists){
        $schema->deploy;        
    }
    $self->load_data($home,$schema);
    return $schema;

}

sub load_data {
    my $self   = shift;
    my $home   = shift;
    my $schema = shift;

    my $dir = $home->rel_file('data/csv');
    my $dp;
    my $fh;
    opendir $dp, $dir;
    while (my $next = readdir $dp){
        next unless $next =~ /\.csv$/;
        my $tbl = $next;
        $tbl    =~ s/\.csv//;
        open $fh, $dir.'/'.$next;
        my $out = csv(in => $fh, headers => "auto", allow_whitespace => 1);
        my $rs  = $schema->resultset($tbl);
        my @pk  = $rs->result_source->primary_columns;
        foreach my $data (@{$out}){
            my @id;
            foreach my $p (@pk){
                push @id, $data->{$p};
            }
            my $row = $rs->find(@id);
            if ($row){
                $row->update($data);                
            }
            else {
                $row = $rs->create($data);
            }
        }
        close $fh;
    }
    closedir $dp;
}

1;
