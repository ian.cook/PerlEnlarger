package Enlarger::Roles::Relay;

use Moose::Role;
use Math::Round qw/round/;
use Time::HiRes qw/time usleep/;

has config  => (is => 'rw');
has schema  => (is => 'rw');

requires qw/init shutdown on time stop status/;

sub _time {
    my $self  = shift;
    my $state = shift;
    my $time  = shift;

    my $old_state = $self->status();

    my $now     = time();
    my $stopped = 0;
    $self->on($state);
    usleep(round($time*1000*1000));

    $self->on($old_state);
}

1;
