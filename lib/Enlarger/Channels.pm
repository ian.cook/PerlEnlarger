package Enlarger::Channels;

use Moose;


use constant {
    FAN       => 0x01,
    FOCUS     => 0x02,
    SAFELIGHT => 0x04,
    GREEN     => 0x08,
    BLUE      => 0x10,
    PREFLASH  => 0x20,
    POSTFLASH => 0x40,
};

sub channel_name {{
        0x01 => 'Fan      ',
        0x02 => 'Focus    ',
        0x04 => 'Safelight',
        0x08 => 'Green    ',
        0x10 => 'Blue     ',
        0x20 => 'Preflash ',
        0x40 => 'Postflash',
    };
}

1;
