package Enlarger::Controller;
use utf8;
use Moose;
use MooseX::NonMoose;
use Math::Round;
use Carp;
extends 'Mojolicious::Controller';

has settings  => (is => 'rw', lazy_build => 1, clearer => '_clear_settings');
has is_test   => (is => 'rw', lazy_build => 1);
has films     => (is => 'rw', lazy_build => 1);
has lenses    => (is => 'rw', lazy_build => 1);
has steps     => (is => 'rw', lazy_build => 1);
has run_steps => (is => 'rw', lazy_build => 1);
has run_step  => (is => 'rw', default => 0);

sub update_setting {
    my $self  = shift;
    my $name  = shift;
    my $value = shift;
    my $prog  = shift || 0;

    my $setting = $self->schema->resultset('Setting')->find($name);
    my $current = $setting->decode($value);

    if ($setting->json){
        $current->[$prog]                 = $value;    
        $self->settings->{$name}->[$prog] = $value;
    }
    else {
        $self->settings->{$name} = $value;
        $current                 = $value;
    }    

    $setting->encode($current);
    $setting->update;
}

sub change_aperture {
    my $self = shift;
    my $new  = shift;

    my $cur  = $self->settings->{'aperture'};
    return unless $new != $cur;
    $self->update_setting('aperture',$new);
    my $diff = Math::Round::nearest(1,6*(log($new) - log($cur)) / log(2))/3;
    for(my $i = 0;$i<(scalar @{$self->settings->{offset_green}});$i++){
        $self->update_setting('offset_green', $self->settings->{offset_green}->[$i] + $diff, $i);
        $self->update_setting('offset_blue',  $self->settings->{offset_blue}->[$i]  + $diff, $i);
    }
}

sub change_lens {
    my $self   = shift;
    my $new_id = shift;
    my $old = $self->schema->resultset('Lens')->find($self->settings->{lens_id});
    my $new = $self->schema->resultset('Lens')->find($new_id);

    my $old_app  = $self->settings->{aperture};
    my $new_apps = [map {$_->aperture} $new->search_related('apertures')->all];

    my @list = sort { abs($a - $old_app) <=> abs($b - $old_app) } @{$new_apps};

    $self->change_aperture($list[0]);
}

sub save_exposure {
    my $self  = shift;
    my $saved = shift;

    my @green;
    my @blue;

    for(my $i=0;$i < @{$self->settings->{offset_green}};$i++){
        push @green, $self->settings->{disable_green}->[$i] ? 'd' : $self->settings->{offset_green}->[$i];    
    }
    for(my $i=0;$i < @{$self->settings->{offset_blue}};$i++){
        push @blue, $self->settings->{disable_blue}->[$i] ? 'd' : $self->settings->{offset_blue}->[$i];    
    }    
    my $exp = $self->schema->resultset('Exposure')->create({
            timestamp => time(),
            roll_id   => $self->settings->{roll_id},
            frame_id  => $self->settings->{frame_id},
            lens_id   => $self->settings->{lens_id},
            aperture  => $self->settings->{aperture},
            height    => $self->settings->{height},
            blue      => \@blue,
            green     => \@green,
            preflash  => $self->settings->{preflash},
            postflash => $self->settings->{postflash},
            saved     => $saved ? 1 : 0,
        });
    return $exp;
}

sub load_exposure {
    my $self = shift;
    my $exp  = shift;

    $self->update_setting('frame_id', $exp->frame_id);
    $self->update_setting('roll_id',  $exp->roll_id);
    $self->update_setting('preflash', $exp->preflash);
    $self->update_setting('postflash',$exp->postflash);    
    $self->change_aperture($exp->aperture);
    my @green;
    my @blue;
    my @green_disable;
    my @blue_disable;
    foreach my $blue (@{$exp->blue}){
        push @blue        , $blue eq 'd' ? $blue : 0;
        push @blue_disable, $blue eq 'd' ? 1     : 0;
    }
    foreach my $green (@{$exp->green}){
        push @green        , $green eq 'd' ? $green : 0;
        push @green_disable, $green eq 'd' ? 1      : 0;
    }
    $self->schema->resultset('settings')->find('disable_green')->update({
            value => JSON::encode(@green_disable)
        });
    $self->schema->resultset('settings')->find('offset_green')->update({
            value => JSON::encode(@green)
        });
    $self->schema->resultset('settings')->find('disable_blue')->update({
            value => JSON::encode(@blue_disable)
        });
    $self->schema->resultset('settings')->find('offset_blue')->update({
            value => JSON::encode(@blue)
        });
    $self->_clear_settings;
}

sub parameters {
    my $self = shift;

    my $green_disp = [];
    my $blue_disp  = [];

    for (my $i=0; $i < (scalar @{$self->settings->{offset_green}});$i++){
        my $green_time    = 2**$self->settings->{offset_green}->[$i] * $self->settings->{base_green};
        my $blue_time     = 2**$self->settings->{offset_blue}->[$i]  * $self->settings->{base_blue};
        $green_disp->[$i] = sprintf("%2.2f (%2.2f)", $green_time, $self->settings->{offset_green}->[$i]);
        $blue_disp->[$i]  = sprintf("%2.2f (%2.2f)", $blue_time, $self->settings->{offset_blue}->[$i]);
    }


    my $p = {
            green_time       => $green_disp,
            blue_time        => $blue_disp,
            green_disable    => $self->settings->{disable_green},
            blue_disable     => $self->settings->{disable_blue},
            base_green       => $self->settings->{base_green},
            base_blue        => $self->settings->{base_green},
            postflash        => $self->settings->{postflash},
            preflash         => $self->settings->{preflash},

            offset_1         => $self->steps->[0],
            offset_2         => $self->steps->[1],
            offset_3         => $self->steps->[2],
            offset_4         => $self->steps->[3],
            exp_flag         => $self->settings->{exp_flag},
            aperture         => $self->settings->{aperture},
            apertures        => [$self->schema->resultset('Aperture')->search({lens_id => $self->settings->{lens_id} })->all],
            lens_id          => $self->settings->{lens_id},
            lenses           => $self->lenses,
            films            => $self->films,
            roll             => $self->schema->resultset('Roll')->find($self->settings->{roll_id}),
            frame            => $self->settings->{frame_id},
            safelight        => $self->relay->get_safelight(),
            focus            => $self->relay->get_focus(),
            test_flag        => $self->settings->{test_flag},
            test_green_mode  => $self->settings->{test_green_mode},
            test_blue_mode   => $self->settings->{test_blue_mode},
            test_steps       => $self->settings->{test_steps} || 5,
            run_step         => $self->run_step,
            is_test          => $self->is_test,
        };
    foreach my $colour (qw/green blue/){
        foreach my $type (qw/start end/){
            my $time                        = 2**$self->settings->{'test_'.$colour.'_'.$type} * $self->settings->{base_green};
            $p->{'test_'.$colour.'_'.$type} = sprintf("%2.2f (%2.2f)", $time, $self->settings->{'test_'.$colour.'_'.$type});
        }
    }        
    $p->{saved}         = $self->schema->resultset('Exposure')->get_saved($p->{roll},$p->{frame},$self->settings->{number_to_save}),
    $p->{roll_readonly} = scalar @{$p->{saved}} > 0;
    $p->{steps}         = $self->run_steps if $self->stash->{tag} && $self->stash->{tag} eq 'run';


    return $p;

}

sub _build_steps {["1","1/3","1/6","1/12"];}    

sub _build_run_steps {
    my $self = shift;
    my $steps = [[-2,-2,-2,-2]];
    if ($self->is_test){
        $steps = $self->_build_test_steps($steps);
    }
    else {
        $steps = $self->_build_prog_steps($steps);
    }
    push @{$steps}, [-1,-1,-1,-1];
    return $steps;
}

sub _build_test_steps {
    my $self = shift;
    my $steps = shift;

    if ($self->settings->{test_flag}){
        return $self->_build_independant_test_steps($steps);        
    }
    else {
        return $self->_build_progressive_test_steps($steps);        
    }
}

sub _build_progressive_test_steps{
    my $self  = shift;
    my $steps = shift;

    my $green_time = 0;
    my $blue_time  = 0;
    for (my $i=0;$i<$self->settings->{test_steps};$i++){
        my $offset;
        my $gt = 0;
        my $bt = 0;
        if ($self->settings->{test_green_mode} == 2){
            $offset      = $self->settings->{test_green_start} + ($self->settings->{test_green_end} - $self->settings->{test_green_start} )*$i/($self->settings->{test_steps}-1);
            $gt          = 2**$offset * $self->settings->{base_green} - $green_time;
            $green_time += $gt;
        }
        if ($self->settings->{test_blue_mode} == 2){
            $offset      = $self->settings->{test_blue_start} + ($self->settings->{test_blue_end} - $self->settings->{test_blue_start} )*$i/($self->settings->{test_steps}-1);
            $bt          = 2**$offset * $self->settings->{base_blue} - $blue_time;
            $blue_time  += $bt;
        }
        $steps->[$self->settings->{test_steps}-$i] = [$gt,$bt,0,0];
    }
    if ($self->settings->{test_green_mode} == 1){
        $green_time    = 2**$self->settings->{test_green_start} * $self->settings->{base_green};
        $steps->[scalar @{$steps} - 1]->[0] = $green_time;
    }
    if ($self->settings->{test_blue_mode} == 1){
        $blue_time    = 2**$self->settings->{test_blue_start} * $self->settings->{base_blue};
        $steps->[scalar @{$steps} - 1]->[1] = $blue_time;
    }



    return $steps;
}

sub _build_independant_test_steps{
    my $self  = shift;
    my $steps = shift;


    for (my $i=0;$i<$self->settings->{test_steps};$i++){
        my $green_time = 0;
        my $blue_time  = 0;
        my $offset;
        if ($self->settings->{test_green_mode} == 2){
            $offset     = $self->settings->{test_green_start} + ($self->settings->{test_green_end} - $self->settings->{test_green_start} )*$i/($self->settings->{test_steps}-1);
            $green_time = 2**$offset * $self->settings->{base_green};
        }
        elsif ($self->settings->{test_green_mode} == 1){
            $green_time    = 2**$self->settings->{test_green_start} * $self->settings->{base_green};
        }
        if ($self->settings->{test_blue_mode} == 2){
            $offset     = $self->settings->{test_blue_start} + ($self->settings->{test_blue_end} - $self->settings->{test_blue_start} )*$i/($self->settings->{test_steps}-1);
            $blue_time = 2**$offset * $self->settings->{base_blue};
        }
        elsif ($self->settings->{test_blue_mode} == 1){
            $blue_time    = 2**$self->settings->{test_blue_start} * $self->settings->{base_blue};
        }
        push @{$steps}, [$green_time,$blue_time,0,0];
    }

    return $steps;
}



sub _build_prog_steps {
    my $self  = shift;
    my $steps = shift;

    for (my $i=0; $i < (scalar @{$self->settings->{offset_green}});$i++){
        my $green_time    = 2**$self->settings->{offset_green}->[$i] * $self->settings->{base_green} unless $self->settings->{disable_green}->[$i];
        my $blue_time     = 2**$self->settings->{offset_blue}->[$i]  * $self->settings->{base_blue}  unless $self->settings->{disable_blue}->[$i];
        if ($self->settings->{exp_flag} == -1){
            push @{$steps}, [$green_time,0,0,0] if $green_time;
            push @{$steps}, [0,$blue_time,0,0]  if $blue_time;
        }
        else {
            push @{$steps}, [$green_time,$blue_time,0,0];
        }
    }

    return $steps;
}

sub _build_is_test {
    my $self = shift;
    return ($self->param('is_test') && $self->param('is_test') == "1") ? 1 : 0;
}

sub _build_settings {
    my $self = shift;
    return { map { $_->name() => $_->decode() } $self->schema->resultset('Setting')->search({})->all };
}

sub _build_lenses {
    my $self = shift;
    return { map { $_->id => $_} $self->schema->resultset('Lens')->search({})->all() };
}

sub _build_films {
    my $self = shift;
    return { map { $_->id => $_ } $self->schema->resultset('Film')->search({})->all() };
}

1;
