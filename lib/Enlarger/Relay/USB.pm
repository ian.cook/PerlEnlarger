package Enlarger::Relay::USB;

use Moose;
use Device::HID::XS qw(:all);
with qw/Enlarger::Roles::Relay/;

has config  => (is => 'rw');
has hid     => (is => 'rw');
has state   => (is => 'rw', default => 0);
has mapping => (is => 'rw');

sub can_offload_time { 0; }

sub init {
    my $self = shift;
    my $hid  = hid_open(hex($self->config->{idVendor}),hex($self->config->{idProduct}),undef) 
        or die "No such device ".$self->config->{idVendor}.':'.$self->config->{idProduct}."!\n"; 

    $self->mapping({
            Enlarger::Channels::FAN()       => $self->config->{fan},
            Enlarger::Channels::FOCUS()     => $self->config->{focus},
            Enlarger::Channels::SAFELIGHT() => $self->config->{safelight},
            Enlarger::Channels::GREEN()     => $self->config->{green},
            Enlarger::Channels::BLUE()      => $self->config->{blue},
            Enlarger::Channels::PREFLASH()  => $self->config->{preflash},
            Enlarger::Channels::POSTFLASH()  => $self->config->{postflash},
        });

    $self->hid($hid);
    $self->on(0);
}

sub on {
    my $self  = shift;
    my $state = shift;
    $self->state($state);

    my $data; 

    if ($state == 0x0 || ($state & 0x7f) == 0x7f){
        # All on/off
        $data = chr($state ? 0xFE : 0xFC);
        hid_send_feature_report($self->hid,$data,1);        
    }
    else {
        for (my $i = 0; $i < 7; $i++){
            my $flg = 2**$i;
            $data  = chr((($flg & $state) == $flg) ? 0xFF : 0xFD);
            $data .= chr($self->mapping->{$flg});
            hid_send_feature_report($self->hid,$data,2);
        }
    }
}

sub status { shift->state;              }
sub time   { shift->_time(shift,shift); }
sub stop   { shift->_stop();            }

sub shutdown {
    my $self = shift;
    $self->on(0);
    hid_close($self->hid);   
}

1;
