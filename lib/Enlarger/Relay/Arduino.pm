package Enlarger::Relay::Arduino;

use Moose;
use Math::Round qw/round/;
use Device::SerialPort;
with qw/Enlarger::Roles::Relay/;

has serial  => (is => 'rw');
has mapping => (is => 'rw');
has r_map   => (is => 'rw');


sub init {
    my $self = shift;
    my $serial = Device::SerialPort->new($self->config->{serialPort});
    $serial->baudrate(9600);
    $serial->databits(8);
    $serial->parity('none');
    $serial->stopbits(1);
    

    $self->mapping({
            Enlarger::Channels::FAN()       => $self->config->{fan},
            Enlarger::Channels::FOCUS()     => $self->config->{focus},
            Enlarger::Channels::SAFELIGHT() => $self->config->{safelight},
            Enlarger::Channels::GREEN()     => $self->config->{green},
            Enlarger::Channels::BLUE()      => $self->config->{blue},
            Enlarger::Channels::PREFLASH()  => $self->config->{preflash},
            Enlarger::Channels::POSTFLASH() => $self->config->{postflash},
        });
    $self->r_map({ reverse %{$self->mapping} });
    $self->serial($serial);   

    $self->_flush();

    $self->send_cmd('?');
    $self->on(0);
}

sub stop { shift->send_cmd('Q'); }


sub on {
    my $self  = shift;
    my $state = shift;

    $self->send_cmd('S',$self->state_to_flag($state));
}

sub time {
    my $self  = shift;
    my $state = shift;
    my $time  = shift;

    $time = round($time*1000);

    $self->send_cmd('T',$self->state_to_flag($state),$time);
}

sub state_to_flag {
    my $self  = shift;
    my $state = shift;

    my $flag = 0;
    for (my $i = 0; $i < 7; $i++){
        my $flg = 2**$i;
        if (($flg & $state) == $flg){
            $flag += 2**($self->mapping->{$flg}-1);
        }
    }
    return $flag;
}

sub status { 
    my $self = shift;
    my $res  = $self->send_cmd('?');

    my @status = split(//,$res);

    my $state  = 0;
    for (my $i = 0; $i < 7; $i++){
        $state += $self->r_map->{$i+1} if $status[$i] eq "1";
    }
    return $state;
}

sub send_cmd {
    my $self  = shift;
    my $cmd   = shift;
    my $state = shift || 0;
    my $time  = shift || 0;

    my $str   = sprintf("<%c %ld %ld>",ord($cmd),$state,$time);

    $self->_flush();

    print STDERR "Sending ".$str."\n";
    $self->serial->write($str);
    my $ret;
    while (!$ret){
        $ret = $self->serial->lookfor();        
    }
    
    print STDERR "Got ".$ret."\n\n";  
    $self->_flush();

    return $ret;
}

sub _flush {
    my $self = shift;
    while($self->serial->lookfor()){}
}

sub shutdown {
    my $self = shift;
    $self->on(0);
}

1;
