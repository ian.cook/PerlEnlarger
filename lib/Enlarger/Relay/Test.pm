package Enlarger::Relay::Test;


use Moose;
use Time::HiRes;
extends qw/Enlarger::Channels/;
with qw/Enlarger::Roles::Relay/;

has file    => (is => 'rw');
has mapping => (is => 'rw');
has relays  => (is => 'rw');

sub init {   
    my $self   = shift;

    $self->file($self->config->{file});
    $self->mapping({
            Enlarger::Channels::FAN()       => $self->config->{fan},
            Enlarger::Channels::FOCUS()     => $self->config->{focus},
            Enlarger::Channels::SAFELIGHT() => $self->config->{safelight},
            Enlarger::Channels::GREEN()     => $self->config->{green},
            Enlarger::Channels::BLUE()      => $self->config->{blue},
            Enlarger::Channels::PREFLASH()  => $self->config->{preflash},
            Enlarger::Channels::POSTFLASH() => $self->config->{postflash},
        });
    $self->on(0);
}

sub on {
    my $self  = shift;
    my $state = shift;

    $self->relays($state);
    open(my $fh, '>', $self->file);
    print $fh $self->channel_name->{0x01}.' (relay '.$self->mapping->{0x01}.') is '.($self->relays & 0x01 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x02}.' (relay '.$self->mapping->{0x02}.') is '.($self->relays & 0x02 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x04}.' (relay '.$self->mapping->{0x04}.') is '.($self->relays & 0x04 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x08}.' (relay '.$self->mapping->{0x08}.') is '.($self->relays & 0x08 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x10}.' (relay '.$self->mapping->{0x10}.') is '.($self->relays & 0x10 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x20}.' (relay '.$self->mapping->{0x20}.') is '.($self->relays & 0x20 ? 'On' : 'Off').".\n";
    print $fh $self->channel_name->{0x40}.' (relay '.$self->mapping->{0x40}.') is '.($self->relays & 0x40 ? 'On' : 'Off').".\n";
    close($fh);
}

sub time     { shift->_time(shift,shift); }
sub stop     { shift->_stop();            }
sub status   { shift->relays;             }
sub shutdown { unlink(shift->file);       }


1;

