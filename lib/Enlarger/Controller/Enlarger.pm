package Enlarger::Controller::Enlarger;
use utf8;
use Moose;
use MooseX::NonMoose;
extends 'Enlarger::Controller';

sub base {
    my $self = shift;

    $self->stash(param => $self->parameters);
    $self->render();
}

sub safelight {
    my $self = shift;

    $self->relay->safelight(1 - $self->relay->get_safelight);
    $self->_clear_settings;
    $self->render(json => $self->parameters);
}

sub focus {
    my $self = shift;

    $self->relay->focus(1 - $self->relay->get_focus);
    $self->_clear_settings;
    $self->render(json => $self->parameters);
}

1;
