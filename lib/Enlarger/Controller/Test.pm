package Enlarger::Controller::Test;
use utf8;
use Moose;
use MooseX::NonMoose;
extends 'Enlarger::Controller';

sub update {
    my $self = shift;

    my $helper = { 'arrow' => 1, 'mode' => 1, 'aperture' => 1};

    my $mth  = $self->param('type');
    if (defined $helper->{$mth}){
        $mth  = '_update_'.$mth;
        $self->$mth();
    }
    $self->_clear_settings;
    $self->render(json => $self->parameters);
}

sub _update_mode {
    my $self = shift;

    my $colour = $self->param('colour');
    my $val    = $self->param('val');
    $self->update_setting('test_'.$colour.'_mode', $val);
}

sub _update_aperture { 
    my $self = shift;
    $self->change_aperture($self->param('aperture'));
}

sub _update_arrow {
    my $self = shift;

    my $dir   = $self->param('dir')   eq 'dw' ? -1 : 1;
    my $field = $self->param('field');
    my $step  = $self->param('step');
    return if $step > 4 || $step < 1;
    $step--;
    my $step_diff = eval $self->steps->[$step];
    $self->update_setting($field, $self->settings->{$field} + $step_diff * $dir);
}

1;