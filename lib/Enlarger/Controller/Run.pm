package Enlarger::Controller::Run;
use utf8;
use Moose;
use MooseX::NonMoose;
extends 'Enlarger::Controller';

sub step {
    my $self = shift;

    $self->is_test($self->param('is_test'));
    $self->run_step($self->param('step'));

    my $steps = $self->run_steps;
    my $nstep = scalar @{$steps};

    if ($self->run_step+1 > $nstep ){ $self->run_step($nstep); }
    if ($self->run_step < 0        ){ $self->run_step(0);      }
   
    my $cur_step = $steps->[$self->run_step];
    if (!defined $cur_step){
        $self->run_step(0);    
    }
    else {    
       $self->run_one_step($cur_step);
    }


    $self->render(json => {run_step => $self->run_step});
}

sub run_one_step {
    my $self = shift;
    my $step = shift;

    return if $step->[0] < 0;

    if ($self->settings->{exp_flag}){
        $self->relay->expose_con($step->[0],$step->[1],$step->[2],$step->[3]);
    }
    else {
        $self->relay->expose_seq($step->[0],$step->[1],$step->[2],$step->[3]);
    }

    $self->render(json => $self->parameters);
}



1;