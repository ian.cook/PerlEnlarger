package Enlarger::Controller::Prog;
use utf8;
use Moose;
use MooseX::NonMoose;
use Math::Round;
extends 'Enlarger::Controller';


sub update {
    my $self = shift;

    my $helper = { 'arrow' => 1, 'disable' => 1, 'aperture' => 1};

    my $mth  = $self->param('type');
    if (defined $helper->{$mth}){
        $mth  = '_update_'.$mth;
        $self->$mth();
    }
    $self->_clear_settings;
    $self->render(json => $self->parameters);
}

sub prog {
    my $self = shift;

    my $type      = $self->param('type');
    my $current   = $self->param('current');
    my $prog_vars = [qw/disable_blue disable_green offset_blue offset_green/];
    my $total     = (scalar @{$self->settings->{offset_green}});

    if ($type eq 'del'){
        foreach my $var (@{$prog_vars}){            
            $self->update_setting($var,undef,$current);
        }
    }
    if ($type eq 'new'){
        foreach my $var (@{$prog_vars}){
            $self->update_setting($var,$self->settings->{$var}->[$total-1],$total);
        }
    }
    $self->render(json => {redirect => '/'});
}

sub save {
    my $self = shift;

    $self->save_exposure(1);
    $self->render(json => {redirect => '/prog'});
}

sub _update_aperture { 
    my $self = shift;
    $self->change_aperture($self->param('aperture'));
}

sub _update_disable {
    my $self = shift;

    my $channel = $self->param('channel');
    my $prog    = $self->param('prog') ? $self->param('prog') : 0;
    my $new     = 1 - $self->settings->{'disable_'.$channel}->[$prog];
    $self->update_setting('disable_'.$channel,$new,$prog);
}

sub _update_arrow {
    my $self = shift;

    my $dir   = $self->param('dir')   eq 'dw' ? -1 : 1;
    my $field = $self->param('field') eq 'green_time' ? 'green' : 'blue';
    my $prog  = $self->param('prog') ? $self->param('prog') : 0;
    my $step  = $self->param('step');
    return if $step > 4 || $step < 1;
    $step--;
    my $step_diff = eval $self->steps->[$step];
    $self->update_setting('offset_'.$field, $self->settings->{'offset_'.$field}->[$prog] + $step_diff * $dir, $prog);
}

1;
