package Enlarger::Controller::Setup;
use utf8;
use Moose;
use MooseX::NonMoose;
extends 'Enlarger::Controller';

sub save {
    my $self = shift;

    $self->_update_roll();
    $self->_update_films();
    $self->_update_lenses();
    $self->_system;
    my $tab = defined $self->param('tab') ? $self->param('tab') : 'roll';
    $self->render(json => {redirect => '/setup#'.$tab});
}

sub _system{
    my $self = shift;

    if (defined $self->param('system') && $self->param('system') eq 'halt'){
        system('/sbin/halt');
    }
    if (defined $self->param('system') && $self->param('system') eq 'reboot'){
        system('/sbin/reboot');
    }

}

sub _update_lenses {
    my $self = shift;

    if ($self->param('new_lens_name') && $self->param('new_lens_app')){
        my $lens = $self->schema->resultset('Lens')->create({
                id   => undef,
                name => $self->param('new_lens_name'),                
            });
        my @apps = split /,/, $self->param('new_lens_app');
        foreach my $app (@apps){
            $app =~ tr/[0-9\.]//dc;
            $lens->create_related('aperture',{
                    lens_id  => $lens->id,
                    aperture => $app
                });
        }
    }

    if ($self->param('del_lens_id')){
        $self->schema->resultset('Aperture')->search({lens_id => $self->param('del_lens_id')})->delete();
        $self->schema->resultset('Lens')->find($self->param('del_lens_id'))->delete();
    }
}

sub _update_films {
    my $self = shift;

    if ($self->param('new_film_name') && $self->param('new_film_iso')){
        $self->schema->resultset('Film')->create({
                id   => undef,
                name => $self->param('new_film_name'),
                iso  => $self->param('new_film_iso')
            });
    }

    if ($self->param('del_film_id')){
        $self->schema->resultset('Film')->find($self->param('del_film_id'))->delete;
    }
}

sub _update_roll {
    my $self = shift;

    my $current_roll = $self->settings->{roll_id};
    my @params = qw/roll_id frame lens_id base_green base_blue exp_flag test_flag/;
    if (defined $self->param('lens_id') && $self->param('lens_id') != $self->settings->{lens_id}){
        $self->change_lens($self->param('lens_id'));
    }

    foreach my $param (@params){
        if (defined $self->param($param)){
            $self->update_setting($param, $self->param($param));
        }
    }
    my $roll;

    if (defined $self->param('new_roll') && $self->param('new_roll') eq 'new_roll'){
        $roll = $self->schema->resultset('Roll')->create({
                ei      => $self->settings->{ei},
                film_id => $self->settings->{film_id},
            });
        $self->update_setting('roll_id',$roll->id);
    }
    else {
        $roll = $self->schema->resultset('Roll')->find($self->settings->{roll_id});
        $roll = $self->schema->resultset('Roll')->create({
                id      => $self->settings->{roll_id},
                ei      => $self->settings->{ei},
                film_id => $self->settings->{film_id},
            }) unless $roll;

        if ($roll->id != $current_roll){
            $self->update_setting('frame',1);
            my @exp = $self->resultset('Exposure')->get_saved($roll,1,1);
            $self->load_exposure($exp[0]) if defined $exp[0];
        }

    }
    $roll->ei($self->param('ei')) if $self->param('ei');
    $roll->film_id($self->param('film_id')) if $self->param('film_id');
    $roll->update;
    $self->update_setting('roll_id',$roll->id);
}


1;