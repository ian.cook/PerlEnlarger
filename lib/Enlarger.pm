package Enlarger;

use Mojo::Base 'Mojolicious';
use Enlarger::Relay;
use Enlarger::DB::Schema;
use Enlarger::DB::Connect;
use DDP;

# This method will run once at server start
sub startup {
    my $self = shift;

    # Load configuration from hash returned by config file
    my $config = $self->plugin('Config');

    # Configure the application
    $self->secrets($config->{secrets});

    my $schema = Enlarger::DB::Connect->new->connect($self->home,$config->{db_file}); 
    my $relay  = Enlarger::Relay->new()->init($config->{relay},$schema);


    $self->helper(relay  => sub { return $relay;  });
    $self->helper(schema => sub { return $schema; });

    # Router
    my $r = $self->routes;

    $r->get('/')->to('enlarger#base', tag => 'prog');
    $r->get('/prog')->to('enlarger#base', tag => 'prog');
    $r->get('/test')->to('enlarger#base', tag => 'test');
    $r->get('/run')->to('enlarger#base', tag => 'run');
    $r->get('/setup')->to('enlarger#base', tag => 'setup'); 


    $r->post('/safelight')->to('enlarger#safelight');
    $r->post('/focus')->to('enlarger#focus');

    $r->post('/setup')->to('setup#save');

    $r->post('/test/update')->to('test#update');

    $r->post('/prog/update')->to('prog#update'); 
    $r->post('/prog/prog')->to('prog#prog');
    $r->post('/prog/save')->to('prog#save');

    $r->post('/run/step')->to('run#step');
    $r->get('/run/step')->to('run#step');

}

1;
